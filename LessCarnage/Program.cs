﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace LessCarnage
{
    class Program
    {
        static void Main(string[] args)
        {
            FileIterator();
            Console.ReadKey();
        }

        static void FileIterator()
        {
            Console.WriteLine("Please enter your Carnage folder (E.g 'D:\\ark\\carnages'):");
            string path = Console.ReadLine();
            int i = 0;
            try
            {
                foreach (string game in Directory.GetDirectories(path))
                {
                    Console.WriteLine(Path.GetFileName(game).ToUpper());
                    foreach (string year in Directory.GetDirectories(game))
                    {
                        foreach (string date in Directory.GetDirectories(year))
                        {
                            if (!Directory.Exists(date + @"\read")) { Directory.CreateDirectory(date + @"\read"); }
                            Console.WriteLine(Path.GetFileName(date));
                            foreach (string file in Directory.GetFiles(date))
                            {
                                DateTime time = File.GetLastWriteTime(file);
                                string newFile = date + @"\read\" + Path.GetFileName(file);
                                if (!File.Exists(newFile))
                                {
                                    Console.WriteLine("Reading " + Path.GetFileName(file));
                                    string player = "Player";
                                    if (!file.Contains("mpc"))
                                    {
                                        player = "PlayerInfo";
                                    }
                                    File.Copy(file, newFile);
                                    XmlShortener(newFile, player);
                                    i++;
                                }
                                else
                                {
                                    Console.WriteLine("{0} already exists", newFile);
                                }
                                try
                                {
                                    File.SetCreationTime(newFile, time);
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e.Message);
                                }
                            }
                        }
                    }
                }
                Console.WriteLine(i + " files shortened");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        static void XmlShortener(string file, string player)
        {
            XDocument doc = XDocument.Load(file);
            List<XElement> players = doc.Root.Element("Players").Elements(player).ToList();

            foreach (XElement plr in players)
            {
                List<XElement> medals = plr.Element("MedalsCount").Elements("Medal").ToList();
                foreach (XElement medal in medals)
                {
                    if (medal.Attribute("mCount").Value == "0")
                    {
                        medal.Remove();
                    }
                }
            }
            string fP = Path.GetDirectoryName(file);
            doc.Save(file);
        }
    }
}
